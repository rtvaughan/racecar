
#include <math.h>

#include "race.h"

int main( int argc, char* argv[] )
{  
  racecar_t* car = race_join( argv[1], RACEHOST, 6379 );
  
  double x=0, y=0, a=0, v=0, w=0; 

  double ranges[16];
  double bearings[16];
  
  while( race_getdata( car, &x, &y, &a, &v, &w, ranges, bearings ) == 0 ) 
    {
      printf( "%s pose %.2f, %.2f, %.2f speed %.2f, %.2f\n", 
	      argv[1],
	      x, y, a, 
	      v, w );

      printf( "ranges: " );	     
      for( int i=0; i<16; i++ )
	printf( "%.2f ", ranges[i] );

      printf( "\nbearings: " );	     
      for( int i=0; i<16; i++ )
	printf( "%.2f ", bearings[i] );

      printf( "\n" );

      
      v=0.1;
      w=0.3;           

      race_sendcmd( car, v, w );      
    }

  race_quit( car );
}
