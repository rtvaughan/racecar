#include <hiredis.h>


#ifndef RACE_H
#define RACE_H

typedef struct {
  redisContext* sub;
  redisContext* pub;
  const char* name;
} racecar_t;

/* Connect to a Race server on the specified host (e.g. "127.0.0.1") and port, and
   create a new car named @carname. Carname is limited to 32
   characters. */
racecar_t* race_join( const char* carname, 
		  const char* hostname, 
		  const int port );

/* Sends a new speed command to the car. Returns 0 on success, or
   error code otherwise. */
int race_sendcmd( racecar_t* car, 
		  double v, double w );

/* Waits for a message from the server containing the latest state of
   the car. Fills in the state variables using their pointers. Returns
   0 on success, or an errorcode otherwise. */
int race_getdata( racecar_t* car, 
		  double* x, double* y, double* a,
		  double* v, double* w,
		  double ranges[],
		  double bearings[] );

/* Disconnects from the server, freeing all resources. */
void race_quit( racecar_t* car );


#endif
