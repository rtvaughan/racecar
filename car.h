
#define RANGER_COUNT 16

typedef struct {
  float v, w;
  float x, y, a;
  float ranges[RANGER_COUNT];
  float bearings[RANGER_COUNT];
} car_data_t;

typedef struct {
  float v, w;
  float x, y, a;
  float ranges[RANGER_COUNT];
  float bearings[RANGER_COUNT];
} car_cmd_t;
