
#include <stdio.h>
#include <stdlib.h>
#include <hiredis.h>
#include <assert.h>
#include <string.h>
#include <signal.h>

#include "race.h"
#include "car.h"


#define VERBOSE 0

racecar_t* g_car; // global car ptr for signal handler

void signal_handler( int s){  
  printf("Caught signal %d\n",s);
  race_sendcmd( g_car, 0, 0 );
  exit(0);   
}

redisContext* race_connect( const char* hostname, const int port )
{
  struct timeval timeout = {2,0};
  
  redisContext* c = redisConnectWithTimeout(hostname, port, timeout);
  if (c == NULL || c->err) {
    if (c) {
      printf("Connection error: %s\n", c->errstr);
      redisFree(c);
    } else {
      printf("Connection error: can't allocate redis context\n");
    }
    exit(1);
  }

  return c;
}


racecar_t* race_join( const char* carname, 
		  const char* hostname,
		  const int port )
  
{
  
  racecar_t* car = malloc( sizeof(racecar_t) );
  car->name = carname; // strndup( carname, (size_t)32 );

#if VERBOSE
  printf( "carname %s hostname %s port %d\n", car->name, hostname, port );
#endif

  car->pub = race_connect( hostname, port ); 
  car->sub = race_connect( hostname, port ); 
  
  // send a command to force the car to exist and/or initialize its speed
  race_sendcmd( car, 0, 0 );

  // catch SIGINT and stop the car right away
  g_car = car;
  signal (SIGINT, signal_handler );

  // wait for reply
  redisReply* reply = redisCommand( car->sub, "SUBSCRIBE car.%s.data", car->name );

#if VERBOSE
  printf("SUBSCRIBE REPLY STR: %s\n", reply->str);
  printf("SUBSCRIBE REPLY INT: %lld\n", reply->integer);

  printf( "Car %s %p %p\n", car->name, car->pub, car->sub );
#endif

  freeReplyObject(reply);  

  return car;
}


int race_publish( racecar_t* car, void* payload, unsigned int len  )
{
  redisReply* reply =
    (redisReply*)redisCommand( car->pub, "PUBLISH car.%s.cmd %b", car->name, payload, len );
  
#if VERBOSE
  printf("PUBLISH REPLY STR: %s\n", reply->str);
  printf("PUBLISH REPLY INT: %lld\n", reply->integer);
#endif
  
  int success = reply->integer == 1;
  
  freeReplyObject(reply);

  return success;
}


int race_sendcmd( racecar_t* car, double v, double w )
{
  char buf[128];
  snprintf( buf, 128, "speed %.2f %.2f", v, w );
  return race_publish( car, buf, strlen(buf));
}

int race_getdata( racecar_t* car, 
		  double* x, double* y, double* a, 
		  double* v, double* w,
		  double ranges[],
		  double bearings[] )
{
  redisReply* reply = NULL;

#if VERBOSE
  printf( "waiting for sub on %s\n", car->name );
#endif

  while( redisGetReply( car->sub, (void**)&reply) == REDIS_OK )
    {      

#if VERBOSE
      for( int i=0; i<reply->elements; i++ )
	printf( "received: %d %s\n", i, reply->element[i]->str );
#endif
      
      if( strncmp( reply->element[0]->str, "message", strlen("message") ) == 0 )
	{								   
	  // parse reply
	  // char* channel = reply->element[1]->str;	 
	  char* data = reply->element[2]->str;

	  
	  
	  /* //puts( data ); */
	  
	  /* double x1, y1, a1, v1, w1; */
	  /* int consumed=0; */
	  /* int matched = sscanf( data,  */
	  /* 			"pose %lf %lf %lf speed %lf %lf br 16 %n",  */
	  /* 			&x1, &y1, &a1, &v1, &w1, &consumed ); */
	  
	  /* assert( matched == 5 ); */


	  /* //char* ranges = strnstr( data, "br", 128 ); */
	  /* //assert( ranges ); */
	  
	  /* //puts( data+consumed ); */

	  
	  /* for( int i=0; i<16; i++ ) */
	  /*   { */
	  /*     data += consumed; */
	  /*     sscanf( data, "%lf,%lf%n", bearings+i, ranges+i, &consumed );  */
	  /*   } */
	  
	  /* //for( int i=0; i<16; i++ ) */
	  /* // printf( "(%.2f,%.2f) ", bearings[i], ranges[i] ); */

	  /* //puts( "\n" ); */

	  car_data_t* c = (car_data_t*)data;
	  
	  if(x) *x = c->x;
	  if(y) *y = c->y;
	  if(a) *a = c->a;
	  if(v) *v = c->v;
	  if(w) *w = c->w;
	  
	  for( int i=0; i<RANGER_COUNT; i++ )
	    {
	      ranges[i] = c->ranges[i];
	      bearings[i] = c->bearings[i];
	    }

	  
	  freeReplyObject(reply);
	  
	  /* if(x) *x = x1; */
	  /* if(y) *y = y1; */
	  /* if(a) *a = a1; */
	  /* if(v) *v = v1; */
	  /* if(w) *w = w1; */




	  
	  return 0; // OK
	}      
    }
  
  puts( "Warning: redisGetReply error" );

  return -1; // read an error from redis
}

void race_quit( racecar_t* car )
{
  if( car )
    {
      if( car->pub )
	redisFree( car->pub );
      
      if( car->sub )
	redisFree( car->sub );

      free( car );
    }
  
}
