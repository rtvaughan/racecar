
RED=hiredis-0.13.3
RACEHOST?="207.23.184.150"

all: car

car: main.c race.c race.h
	cc -static -std=c99 -DRACEHOST=\"$(RACEHOST)\" -Wall -g -I$(RED) -L$(RED) race.c main.c -lhiredis -lm -o $@

clean:
	rm -f car
	rm -f *.o

dep:
	wget https://github.com/redis/hiredis/archive/v0.13.3.tar.gz 
	tar xzvf v0.13.3.tar.gz
	cd hiredis-0.13.3 && $(MAKE)
